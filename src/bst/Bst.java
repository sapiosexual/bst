package bst;

import java.awt.Graphics;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author Admin
 */
public class Bst<E> {

    BstNode<E> root = null;
    
    public void add(Pair<E> keyValue) {
        if(root == null) {
            root = new BstNode(keyValue, null, this);
        } else {
            root.add(keyValue);
        }
    }
    
    public boolean delete(int key) {
        if(root == null) return false;
        return root.delete(key);
    }
            
    public E find(int key) {
        if(root == null) return null;
        return root.find(key);
    }
    
    public int height() {
        if(root == null) return -1;
        return root.computeHeight(0);
    }
    
    public int size() {
        if(root == null) return 0;
        return root.size(0);
    }
    
    public static void somethingLikeBinarySearch(int[] array, Bst tree, int start, int end) {
        if(end - start <= 0) return;
        int middle = (start + end)/2;
        tree.add(new Pair(array[middle], new Object()));
        System.out.println(tree.height());
        somethingLikeBinarySearch(array, tree, start, middle);
        somethingLikeBinarySearch(array, tree, middle + 1, end);
    }
    
    public void paint(Graphics g) {
        if(root != null) root.paint(g, 0, height());
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17,
                       18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31};
        Bst tree1 = new Bst();
        /*for(int i = 0; i < 10; i++) {
            tree1.add(new Pair(i, new Object()));
            System.out.println(tree1.height());
        }*/
        somethingLikeBinarySearch(array, tree1, 0, array.length);
        for(int i = 0; i < 10; i+=2) {
            tree1.delete(i);
            System.out.println(tree1.height());
        }
        JFrame frame = new JFrame();
        frame.setContentPane(new TreePainter(tree1));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setExtendedState(frame.getExtendedState() | frame.MAXIMIZED_BOTH);
        JButton buttonGen = new JButton("gen");
        buttonGen.addActionListener((a) -> {
            TreePainter painter = (TreePainter)frame.getContentPane();
            tree1.add(new Pair(painter.getIPlus(), new Object()));
            frame.repaint();
        });
        JButton buttonUngen = new JButton("ungen");
        buttonUngen.addActionListener((a) -> {
            TreePainter painter = (TreePainter)frame.getContentPane();
            tree1.delete(painter.getIMinus());
            frame.repaint();
        });
        GroupLayout layout = new GroupLayout(frame.getContentPane());
        layout.setHorizontalGroup(layout.createSequentialGroup().addComponent(buttonGen, 15, 1300, 1500)
        .addComponent(buttonUngen, 15, 1300, 1500));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(buttonGen, 20, 20, 20)
        .addComponent(buttonUngen, 20, 20, 20));
        
        frame.setVisible(true);
    }
    
    
}
