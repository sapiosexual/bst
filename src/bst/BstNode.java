package bst;

import java.awt.Graphics;

/**
 *
 * @author Admin
 */
public class BstNode<E> {
    Pair<E> keyValue;
    Bst<E> tree;
    BstNode<E> right;
    BstNode<E> left;
    BstNode<E> parent;
    
    int OVAL_BOUND = 20;
    int x;
    int y;

    public BstNode(Pair<E> keyValue, BstNode parent, Bst tree) {
        this.keyValue = keyValue;
        this.parent = parent;
        this.tree = tree;
    }
    
    public void add(Pair<E> i) {
        BstNode<E> node;
        if(keyValue.key > i.key) {
            if(left != null) {
                left.add(i);
            } else {
                left = new BstNode(i, this, tree);
            }
            node = left;
        } else {
            if(right != null) {
                right.add(i);
            } else {
                right = new BstNode(i, this, tree);
            }
            node = right;
        }
    }
    
    public boolean delete(int key) {
        boolean result = false;
        if(keyValue.key == key) {
            //If we have two childs we looking for right child's the most left descendant(rctmld).
            //If right child does not have left child we just shift this node to this place.
            //If we founded rctmld we just change value of current node to value of
            //rctmld then call delete() on rctmld
            result = true;
            if(right != null && left != null) {
                if(right.left != null) {
                    BstNode current = right.left;
                    while(current.left != null) {
                        current = current.left;
                    }
                    keyValue = current.keyValue;
                    current.delete(keyValue.key);
                } else {
                    right.left = left;
                    left.parent = right;
                    right.parent = parent;
                    if(parent.right == this) {
                        parent.right = right;
                    } else {
                        parent.left = right;
                    }
                }
            } else {
                //otherwise we just delete this node and add tail of this node
                //to parent, if it neccesary
                BstNode child = null;
                if(right != null) child = right;
                if(left != null) child = left;
                if(child != null) child.parent = parent;
                if(parent == null) {
                    tree.root = child;
                } else {
                    if(parent.right == this) {
                        parent.right = child;
                    } else {
                        parent.left = child;
                    }
                    parent = null;
                }
            }
        } else if(keyValue.key > key) {
            if(left != null) {
                result = left.delete(key);
            }
        } else {
            if(right != null) {
                result = right.delete(key);
            }
        }
        return result;
    }
    
    public E find(int key) {
        if(keyValue.key == key) return keyValue.value;
        if(keyValue.key > key && left != null) {
            return left.find(key);
        }
        if(keyValue.key < key && right != null) {
            return right.find(key);
        }
        return null;
    }
    
    public boolean isLeaf() {
        return left == null && right == null;
    }
    
    private boolean onlyRightExists() {
        return left == null && right != null;
    }
    
    private boolean onlyLeftExists() {
        return left != null && right == null;
    }
    
    public int computeHeight(int level) {
        if(isLeaf()) return level;
        if(onlyLeftExists()) return left.computeHeight(level + 1);
        if(onlyRightExists()) return right.computeHeight(level + 1);
        return Math.max(left.computeHeight(level + 1), right.computeHeight(level + 1));
    }
    
    public int size(int size) {
        if(left != null) size = left.size(size);
        if(right != null) size = right.size(size);
        return ++size;
    }
    
    public void paint(Graphics g, int level, int height) {
        if(level < 0) return;
        if(level == 0) {
            y = 100;
            x = g.getClipBounds().width/2;
            g.drawOval(x-OVAL_BOUND/2, y-OVAL_BOUND/2, OVAL_BOUND, OVAL_BOUND);
            g.drawString(keyValue.key.toString(), x - 8, y + 5);
        } else {
            int pieceYSize = (g.getClipBounds().height - 200)/height;
            int pieceXSize = g.getClipBounds().width/((int)Math.pow(2, level));
            int parentXShift, parentYShift;
            parentXShift = parentYShift = 8;
            if(parent.left == this) {
                pieceXSize*=-1;
                parentXShift*=-1;
            }
            x = parent.x + pieceXSize/2;
            y = parent.y + pieceYSize;
            g.drawOval(x-OVAL_BOUND/2, y-OVAL_BOUND/2, OVAL_BOUND, OVAL_BOUND);
            g.drawString(keyValue.key.toString(), x - 8, y + 5);
            g.drawLine(x, y - OVAL_BOUND/2, 
                       parent.x + parentXShift, parent.y + parentYShift);
        }
        if(right != null) right.paint(g, level + 1, height);
        if(left != null) left.paint(g, level + 1, height);
    }
}
