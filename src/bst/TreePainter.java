package bst;

import java.awt.Graphics;
import javax.swing.JPanel;

/**
 *
 * @author Admin
 */
public class TreePainter extends JPanel {

    private Bst tree;
    int increment = 10000;
    int decrement = 10000;
    public TreePainter(Bst tree) {
        this.tree = tree;
    }

    public void setTree(Bst tree) {
        this.tree = tree;
    }

    public Bst getTree() {
        return tree;
    }
    
    public int getIPlus() {
        return increment--;
    }
    
    public int getIMinus() {
        return decrement--;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        tree.paint(g.create());
    }
    
}
