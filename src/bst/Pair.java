package bst;

/**
 *
 * @author Admin
 */
public class Pair<E> {
    Integer key;
    E value;

    public Pair(Integer key, E value) {
        this.key = key;
        this.value = value;
    }
}
